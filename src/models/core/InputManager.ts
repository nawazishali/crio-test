import Store from "../../store";
import Polygon from "../elements/Polygon";
import RENDER_PROPS from "../../constants/RenderProps";
import IPoint from "../../interfaces/IPoint";
import KeyCodes from "../../enums/KeyCodes";

export default class InputManager {

  private canvas: HTMLCanvasElement;
  private store: Store;
  private isMouseDown!: boolean;

  constructor(canvas: HTMLCanvasElement) {
    this.canvas = canvas;
    this.store = Store.getInstance();

    canvas.addEventListener("mousedown", event => this.handleMouseDown(event));
    canvas.addEventListener("mouseup", event => this.handleMouseUp(event));
    canvas.addEventListener("mousemove", event => this.handleMouseMove(event));
    window.addEventListener("keydown", event => this.handleKeyboardEvent(event));
  }

  private handleMouseDown = (e: MouseEvent) => { 
    this.isMouseDown = true;
    let ex = e.clientX;
    let ey = e.clientY;
    this.store.getElements().forEach((polygon) => {
      if(polygon.selected({x: ex, y: ey})) {
        polygon.props = RENDER_PROPS.SELECTED_POLYGON_OUTLINE;
        this.store.selectElement(polygon);
      } else {
        polygon.props = RENDER_PROPS.POLYGON_OUTLINE;
        this.store.deselectElement(polygon);
      }
    })
  }

  private handleMouseUp = (e: MouseEvent) => { 
    this.isMouseDown = false;
  }

  private handleMouseMove = (e: MouseEvent,) => {
    if(this.isMouseDown) {
      this.store.getSelectedElements().forEach((polygon) => {
        let ex = e.clientX - polygon.selectionPoint.x;
        let ey = e.clientY - polygon.selectionPoint.y;
        polygon.points = polygon.points.map((point) => {
          return { x: ex + point.x, y: ey + point.y };
        })
        polygon.selectionPoint.x += ex;
        polygon.selectionPoint.y += ey;
  
      })
    }
  }

  private handleKeyboardEvent = (e: KeyboardEvent) => {
    if(e.keyCode === KeyCodes.C) {
      let i = Math.floor(Math.random() * 50);
      this.store.createElement(new Polygon([{ x: 50 + i, y: 50 + i }, { x: 350 + i, y: 50 + i }, { x: 350 + i, y: 150 + i }, { x: 50 + i, y: 150 + i }]));
    }
  }

}
