import IPoint from "../../interfaces/IPoint";
import IRenderProps from "../../interfaces/IRenderProps";
import Element from "./Element";
import RenderingUtils from "../../utils/rendering";

import RENDER_PROPS from "../../constants/RenderProps";
import { render } from "react-dom";

export default class Polygon extends Element {
  points: IPoint[];
  selectionPoint!: IPoint;
  props!: IRenderProps;

  constructor(points: IPoint[]) {
    super();
    this.points = points;
    this.props = RENDER_PROPS.POLYGON_OUTLINE;
  }

  draw = (context: CanvasRenderingContext2D) => {
    RenderingUtils.renderLine(context, this.points, this.props);
  }

  selected = (point: IPoint) => {
    // ray-casting algorithm
    let vs = this.points;
    const {x, y} = point;
    var inside = false;
    for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
      //Navigating clockwise on everyside of polygon
      var xi = vs[i].x, yi = vs[i].y;
      var xj = vs[j].x, yj = vs[j].y;
      //checking to see if ray intersects current side
      var intersect = ((yi > y) != (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
      if (intersect) inside = !inside;
    }

    if(inside) {
      this.selectionPoint = point;
    }
    //No need to count evens or odds cause the boolean is updated incrementally
    return inside;
  };
}
