import shortid from "shortid";
import _ from "lodash";

import IDrawable from "../../interfaces/IDrawable";
import IRenderProps from "../../interfaces/IRenderProps";
import IPoint from "../../interfaces/IPoint";

export default abstract class Element implements IDrawable {
  id: string;
  props!: IRenderProps;
  points!: IPoint[];
  selectionPoint!: IPoint;

  constructor() {
    this.id = shortid.generate();
  }

  abstract draw(context?: CanvasRenderingContext2D): void;
  abstract selected(point?: IPoint): boolean;
}