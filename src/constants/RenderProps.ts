/**
 * Defines system render props
 *
 */

const RENDER_PROPS = {
  POLYGON_OUTLINE: { border: { color: "#aaa", width: 1 }, close: true },
  SELECTED_POLYGON_OUTLINE: { border: { color: "red", width: 1 }, close: true },
};

export default RENDER_PROPS;
